import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoginForm from './LoginForm';
import doLoginAction from '../../redux/actions/login';


class LoginContainer extends Component {
  render() {
    return (
      <LoginForm
        doLogin={this.props.doLogin}
        loading={this.props.loading}
      />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  doLogin: (email, password) => dispatch(doLoginAction(email, password))
});
  
const mapStateToProps = state => ({
  loading: state.loading
});
  
export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);